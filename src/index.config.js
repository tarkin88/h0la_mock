'use strict';

const config = ($mdThemingProvider, RestangularProvider, $sceDelegateProvider, $mdIconProvider, $urlRouterProvider) => {

    $mdThemingProvider.theme('Ho1aTheme')
        .primaryPalette('green')
        .accentPalette('grey', {
            'default': '800'
        });
    $mdThemingProvider.setDefaultTheme('Ho1aTheme');
    /* Habilita el color en headers de chrome mobile */
    $mdThemingProvider.enableBrowserColor({
        theme: 'Ho1aTheme'
    });
    RestangularProvider.setBaseUrl('http://beta.json-generator.com/api/json/');

    $mdIconProvider
        .defaultFontSet('fa', 'https://use.fontawesome.com/501327223b.js');

    $urlRouterProvider.otherwise(function($injector) {
        var $state = $injector.get('$state');
        $state.go('login');
    });
};

export default config;