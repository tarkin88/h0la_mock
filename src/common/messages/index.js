'use strict';
import template from './messages.html';

export const messages = {
    bindings: {
        object: '='
    },
    template,
};