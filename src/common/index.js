'use strict';
import { messages } from './messages';
import { navigation } from './navigation';

const common = angular
    .module('app.common', [])
    .component('navigation', navigation)
    .component('messages', messages)
    .name;

export default common;