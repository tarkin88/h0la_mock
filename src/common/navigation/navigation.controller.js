'use strict';

class NavController {
	constructor($mdSidenav) {
		this.$mdSidenav = $mdSidenav;
	}

	$onInit() {
		this.data = {
			title: 'Expenses 360 °',
			user: {
				name: 'Francisco Suárez',
				email: 'eric.suarez@deintec.com'
			},
			sidenav: {
				sections: [{
					name: 'Principal',
					actions: [{
						icon: 'home',
						name: 'Inicio',
						link: 'home'
					},
						{
							icon: 'person',
							name: 'Login',
							link: 'login'
						}
					]
				}]
			}
		};
	}

	togglemenu(menu) {
		this.$mdSidenav(menu).toggle();
		if (menu === 'right') {
			this.notificationBar();
		}
	}

	notificationBar() {
		this.today = new Date();
		this.notifications = [
			{
				'title': 'Comprobación de gastos',
				'state': 'lists',
				'body': '#3421 [rechazada]'
			},
			{
				'title': 'Comprobación de gastos',
				'state': 'home',
				'body': '#3422 [aprobada]'
			},
			{
				'title': 'Comprobación de gastos',
				'state': 'buttons',
				'body': '#34222 [aprobada]'
			}
		]
	}
}

NavController.$inject = ['$mdSidenav'];
export default NavController;