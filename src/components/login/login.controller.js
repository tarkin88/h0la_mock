'use strict';

class LoginController {
    constructor(Restangular, $state, $mdToast) {
        this.rest = Restangular;
        this.state = $state;
        this.toast = $mdToast;
    }
    login(data) {
        this.login_promise = this.rest.all('get/Nk3jcUfjz').post({ username: data.username, password: data.password })
            .then(response => {

                this.state.go('home');
                this.toast.show(
                    this.toast.simple()
                    .textContent('Bienvenido!')
                    .action('Cerrar')
                    .position('bottom right')
                    .hideDelay(4000)
                );
            }, error => {
                this.toast.show($mdToast.simple().textContent('ERROR!'));
            });
    }
};

LoginController.$inject = ['Restangular', '$state', '$mdToast'];
export default LoginController;