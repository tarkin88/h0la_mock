'use strict';
import { LoginComponent } from './login.component';
import './login.scss';

const login = angular
    .module('login', [])
    .component('login', LoginComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('login', {
                url: '/',
                component: 'login'
            });
        $urlRouterProvider.otherwise('/');
    })
    .name;

export default login;