'use strict';

import controller from './login.controller';
import template from './login.html';

export const LoginComponent = {
    bindings: {},
    controller,
    template
};