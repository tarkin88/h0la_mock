'use strict';
import {FormsComponent} from './forms.component';

const forms = angular
    .module('forms', [])
    .component('forms', FormsComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('forms', {
                url: '/forms',
                component: 'forms'
            });
        $urlRouterProvider.otherwise('/');
    })
    .name;

export default forms;