'use strict';
import template from './forms.html';
import controller from './forms.controller';

export const FormsComponent = {
    bindings: {
        data: '<'
    },
    controller,
    template
};