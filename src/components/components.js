'use strict';
import Home from './home';
import Buttons from './buttons';
import Forms from './forms';
import Lists from './lists';
import Cards from './cards';
import Viajes from './listas-viajes';
import Login from './login';

const components = angular
    .module('app.components', [
        Home,
        Buttons,
        Forms,
        Lists,
        Cards,
        Viajes,
        Login
    ])
    .name;
export default components;