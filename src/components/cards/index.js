'use strict';
import {CardsComponent} from './cards.component';

const cards = angular
    .module('cards', [])
    .component('cards', CardsComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('cards', {
                url: '/cards',
                component: 'cards'
            });
        $urlRouterProvider.otherwise('/');
    })
    .name;

export default cards;