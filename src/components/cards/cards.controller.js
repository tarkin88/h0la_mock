'use strict';
class CardsController {

	constructor() {
	}

	$onInit() {
		this.imagePath = [
			'https://static.pexels.com/photos/141635/pexels-photo-141635.jpeg',
			'https://static.pexels.com/photos/34624/pexels-photo.jpg',
			'https://static.pexels.com/photos/163037/london-street-phone-cabin-163037.jpeg'
		];
	}
}

export default CardsController;