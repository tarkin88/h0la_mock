'use strict';
import template from './cards.html';
import controller from './cards.controller';

export const CardsComponent = {
	bindings: {
		data: '<'
	},
	controller,
	template
};

