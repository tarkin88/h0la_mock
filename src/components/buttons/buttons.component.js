'use strict';
import template from './buttons.html';
import controller from './buttons.controller';

export const ButtonsComponent = {
    bindings: {
        data: '<'
    },
    controller,
    template
};