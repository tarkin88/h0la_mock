'use strict';
import {ButtonsComponent} from './buttons.component';

const buttons = angular
    .module('buttons', [])
    .component('buttons', ButtonsComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('buttons', {
                url: '/buttons',
                component: 'buttons'
            });
        $urlRouterProvider.otherwise('/');
    })
    .name;

export default buttons;