'use strict';

class ListsController {
    constructor() {
        this.data = {};
    }

    $onInit() {
        this.data.users = [
            {
                name: 'Usuario 1',
                rol: 'Administrador',
                img_url: 'http://ievdigital.com.br/blog/wp-content/uploads/2015/06/marketing-cliente-potencial-avatar.jpg',
                date: new Date()
            },
            {
                name: 'Usuario 2',
                rol: 'Gerencia',
                img_url: 'http://paginawebmedia.com/wp-content/uploads/avatar-7.png',
                date: new Date()
            },
            {
                name: 'Usuario 3',
                rol: 'Desarrollo',
                img_url: 'http://dashp.com/wp-content/uploads/2014/11/1024x1024xflat-faces-icons-square-man-9.png.pagespeed.ic.2sZHyAuKGM.png',
                date: new Date()
            },
        ];
    }
}

export default  ListsController;