'use strict';
import template from './lists.html';
import controller from './list.controller';

export const ListsComponent = {
    bindings: {},
    template,
    controller

};