'use strict';
import {ListsComponent} from './lists.component';

const lists = angular
    .module('lists', [])
    .component('lists', ListsComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('lists', {
                url: '/lists',
                component: 'lists'
            });
        $urlRouterProvider.otherwise('/');
    })
    .name;

export default lists;