'use strict';
import {ViajesComponent} from './viajes.component';
import { lista } from './lista';

const listaViajes = angular
	.module('viajes', [])
	.component('lista', lista)
	.component('viajes', ViajesComponent)
	.config(($stateProvider, $urlRouterProvider) => {
		$stateProvider
			.state('viajes', {
				url: '/viajes',
				component: 'viajes'
			});
		$urlRouterProvider.otherwise('/');
	})
	.name;

export default listaViajes;