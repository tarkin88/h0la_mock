'use strict';
class ListaController {
    constructor(Restangular) {
        this.rest = Restangular;
        this.offset = 0;
    }

    $onInit() {

    }
    getUsers(page, pageSize) {
        const offset = (page - 1) * pageSize;
        return this.rest.all('get/Nk3jcUfjz').getList({
                'offset': offset,
                'limit': pageSize,
            })
            .then(response => {
                return {
                    results: response,
                    totalResultCount: response.length,
                };

            });
    }

    selectedRowCallback(rows) {
        console.log(rows);
    }
    deleteRowCallback(rows) {
        console.log(rows);
    }
}
ListaController.$inject = ['Restangular'];
export default ListaController;