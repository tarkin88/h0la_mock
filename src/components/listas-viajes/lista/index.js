'use strict';
import template from './lista.html';
import controller from './lista.controller';
import './lista.scss';

export const lista = {
	template,
	controller
};