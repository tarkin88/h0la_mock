'use strict';
/* libraries */
import angular from 'angular';
import ngAria from 'angular-aria';
import ngAnimate from 'angular-animate';
import ngMaterial from 'angular-material';
import ngRoute from 'angular-ui-router';
import Restangular from 'restangular';
import cgBusy from '@cgross/angular-busy';
import ngSanitize from 'angular-sanitize';
import mdDataTable from 'md-data-table';
import ngMdIcons from 'angular-material-icons';
import 'angular-i18n/angular-locale_es-mx';

import 'md-data-table/dist/md-data-table-templates';
/* common */
import common from './common';
import {appComponent} from './common/main.component';
/* components */
import components from './components/components';
/* Config */
import config from './index.config';
/* Run */
import run from './index.run';
/* Styles */
import 'angular-material/angular-material.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.scss';
import 'md-data-table/dist/md-data-table-style.css';
import '@cgross/angular-busy/dist/angular-busy.min.css';

const app = angular
	.module('angularApp', [
		ngAria,
		ngAnimate,
		ngMaterial,
		ngMdIcons,
		ngRoute,
		Restangular,
		ngSanitize,
		mdDataTable,
		cgBusy,
		common,
		components
	])
	.component('app', appComponent)
	.config(config)
	.run(run)
	.name;
/* Bootstraping */
document.addEventListener('DOMContentLoaded', () => angular.bootstrap(document, ['angularApp']));

export default app;